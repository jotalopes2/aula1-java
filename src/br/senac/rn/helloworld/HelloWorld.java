package br.senac.rn.helloworld;

public class HelloWorld {

    public static void main(String[] args) {

        // Declaração de variáveis de tipos primitivos

        int valor1 = 2;       // Variável numérica do tipo inteiro
        long idade = 32L;     // Variável numérica do tipo inteiro
        short valor2 = 33;    // Variável numérica do tipo inteiro

        float nota1 = 8.5f;   // Variável numérica do tipo decimal
        double nota2 = 10.2f; // Variável numérica do tipo decimal

        boolean ehPrimo = true; // Variável do tipo lógico

        char letra = 'd'; // Variável do tipo literal (caractere)

        // Classes que substituem os tipos primitivos

        Integer valor3 = 2;
        Long valor4 = 2L;

        Float nota3 = 8.5f;
        Double nota4 = 7.5;

        Boolean ehNumPrimo = true;

        Character letra2 = 'd';
        String frase = "Oi mundo"; // Tipo literal (texto)


        System.out.println("Bem vindo ao mundo JAVA!!!");

    }

}
